<?php

declare(strict_types=1);

namespace Zlf\Unit;

use Exception;

class Xml
{

    /**
     * XML转数组
     * @param string $xml
     * @return array
     */
    public static function toArray(string $xml): array
    {
        try {
            if (strlen($xml)) {
                return json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
            } else {
                return [];
            }
        } catch (Exception $exception) {
            return [];
        }
    }
}