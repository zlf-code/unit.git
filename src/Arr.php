<?php
declare(strict_types=1);

namespace Zlf\Unit;

/**
 * 数组工具类
 */
class Arr
{
    /**
     * 无限多维数组合并,在原有的基础上由右面的值合并前面的值
     * @param ...$arrays
     * @return array
     * @author 2021-06-01 22:47:58
     */
    public static function merge(...$arrays): array
    {
        $array = [];
        foreach ($arrays as $item) {
            if (!is_array($item)) return $item;
            foreach ($item as $key => $info) {
                if (is_array($info)) {
                    if (isset($array[$key]) && $array[$key]) {
                        $array[$key] = self::merge($array[$key], $info);
                        continue;
                    }
                }
                $array[$key] = $info;
            }
        }
        return $array;
    }


    /**
     * 获取数组父子数据结构
     * @param $array
     * @param string $findName
     * @param string $pid
     * @param string $child
     * @param int|string $root
     * @return array
     * @author 竹林风@875384189 2021/3/1 15:55
     */
    public static function getTree($array, string $findName = 'id', string $pid = 'pid', string $child = 'children', $root = 0): array
    {
        $tree = [];
        $packData = [];
        foreach ($array as $data) {
            $packData[$data[$findName]] = $data;
        }
        foreach ($packData as $key => $val) {
            if ($val[$pid] == $root) {
                $tree[] = &$packData[$key];
            } else {
                $packData[$val[$pid]][$child][] = &$packData[$key];
            }
        }
        return $tree;
    }


    /**
     * 数组MD5
     * @param array $array
     * @param bool $len_16
     * @return string
     */
    public static function md5(array $array, bool $len_16 = false): string
    {
        $json = http_build_query($array);
        return $len_16 ? substr(md5($json), 8, 16) : md5($json);
    }


    /**
     * 提取指定元素值为一个新的列表元素
     * @param $array
     * @param null $key
     * @return array
     * @author 竹林风@875384189 2021/3/11 16:44
     */
    public static function getValues($array, $key = null): array
    {
        $values = [];
        foreach ($array as $item) {
            if (is_object($item)) {
                if (is_null($key)) {
                    $values[] = $item;
                } else {
                    $values[] = $item->$key;
                }
            } elseif (is_array($item)) {
                if (is_null($key)) {
                    $values[] = $item;
                } else {
                    $values[] = $item[$key];
                }
            }

        }
        return $values;
    }


    /**
     * 数组转键值对数据
     * @param $array
     * @param string $keyName
     * @param string|null $valName
     * @return array
     * @author 竹林风@875384189 2021/7/13 18:17
     */
    public static function map($array, string $keyName = 'id', ?string $valName = 'name'): array
    {
        $data = [];
        foreach ($array as $row) {
            $row = Obj::toArray($row);
            if (is_null($valName)) {
                $data[$row[$keyName]] = $row;
            } else {
                $data[$row[$keyName]] = ($valName === '*' ? $row : $row[$valName]);
            }
        }
        return $data;
    }


    /**
     * 重置一维数组键名为数组自然排序
     * @param array $array
     * @return array
     */
    public static function resetSort(array $array): array
    {
        $new = [];
        foreach ($array as $item) {
            $new[] = $item;
        }
        return $new;
    }


    /**
     * 深度获取数组数据
     * @param array $array
     * @param string $key
     * @param null $default
     * @return array|mixed|null
     */
    public static function get(array $array, string $key, $default = null)
    {
        $keys = Str::explode('.', trim($key));
        foreach ($keys as $k) {
            if (is_array($array) && isset($array[$k])) {
                $array = $array[$k];
            } else {
                return $default;
            }
        }
        return $array;
    }


    /**
     * 截取指定长度数组
     * @param array $array 数组
     * @param int $start 开始位置
     * @param int $count 截取长度
     * @return array  返回的将是1维数组
     */
    public static function subArray(array $array, int $start, int $count): array
    {
        $arr = [];
        $index = 0;
        foreach ($array as $value) {
            if ($start >= $index) $arr[] = $value;
            if ($count === count($arr)) return $arr;
        }
        return $arr;
    }


    /**
     * 获取数组最后一个元素值
     * @param array $array
     * @return mixed
     */
    public static function finalValue(array $array)
    {
        $value = null;
        foreach ($array as $item) {
            $value = $item;
        }
        return $value;
    }


    /**
     * 获取第一个元素值
     * @param array $array
     * @return mixed
     */
    public static function firstValue(array $array)
    {
        foreach ($array as $item) {
            return $item;
        }
        return null;
    }


    /**
     * 获取数组最后一个键名称
     * @param array $array
     * @return int|string
     */
    public static function finalKey(array $array)
    {
        $keyName = null;
        foreach ($array as $key => $item) {
            $keyName = $key;
        }
        return $keyName;
    }


    /**
     * 获取数组第一个元素名
     * @param array $array
     * @return int|string|null
     */
    public static function firstKey(array $array)
    {
        foreach ($array as $key => $item) {
            return $key;
        }
        return null;
    }


    /**
     * 判断数组是列表还是字典
     * @param array $arr
     * @return string
     */
    public static function type(array $arr): string
    {
        return Is::list($arr) ? 'list' : 'map';
    }

    /**
     * 接收指定参数
     * @param array $array
     * @param array $keys
     * @return array
     */
    public static function receive(array $array, array $keys): array
    {
        $data = [];
        foreach ($keys as $item) {
            if (is_array($item)) {
                if (isset($array[$item[0]])) {
                    $data[$item[0]] = $array[$item[0]];
                } elseif (isset($item[1])) {
                    $data[$item[0]] = $item[1];
                }
            } else {
                if (isset($array[$item])) {
                    $data[$item] = $array[$item];
                }
            }
        }
        return $data;
    }


    /**
     * 删除多维数组中的空元素
     * @author 竹林风@875384189 2022/6/2 9:51
     */
    public static function removeEmptyElements($arr)
    {
        foreach ($arr as $k => $v) {
            if (is_array($v) && count($v) > 0) {
                $arr[$k] = self::removeEmptyElements($v);
            }
            if (empty($v)) unset($arr[$k]);
        }
        return $arr;
    }


    /**
     * 连接列表
     * @param array $list
     * @param string $implode
     * @return string
     */
    public static function join(array $list, string $implode = '', bool $empty_skip = true): string
    {
        $listData = [];
        foreach ($list as $value) {
            if (Is::empty($value) && $empty_skip === true) {
                continue;
            }
            $listData[] = $value;
        }
        return implode($implode, $listData);
    }


    /**
     * 数组追加元素
     * @param array $array
     * @param mixed ...$data
     * @return array
     */
    public static function push(array $array, ...$data): array
    {
        foreach ($data as $item) {
            $array[] = $item;
        }
        return $array;
    }


    /**
     * 判断数组中是否存在某个元素
     * @param mixed $value
     * @param array $array
     * @return bool
     */
    public static function exists($value, $array): bool
    {
        return in_array($value, $array, true);
    }


    /***
     * 数组去重,只支持一纬数组
     */
    public static function unique(array $arr): array
    {
        $news = [];
        foreach ($arr as $item) {
            if (self::exists($item, $news) === false) {
                $news[] = $item;
            }
        }
        return $news;
    }


    /**
     * 删除数组中的元素
     * @param array $array
     * @param array $keys
     * @return array
     */
    public static function remove(array $array, array $keys): array
    {
        foreach ($keys as $key) {
            unset($array[$key]);
        }
        return $array;
    }


    /**
     * 修改数组映射关系
     * @param array $list
     * @param array $mapping
     * @return array
     */
    public static function KeyMapping(array $list, array $mapping): array
    {
        $data = [];
        foreach ($list as $item) {
            $row = [];
            foreach ($mapping as $k => $v) {
                $row[$v] = $item[$k] ?? null;
            }
            $data[] = $row;
        }

        return $data;
    }

    /**
     * 删除空元素
     * @param array $list
     * @param array $items 指定需要排除的元素
     * @return array
     */
    public static function removeEmptyValue(array $list, array $items = []): array
    {
        $data = [];
        foreach ($list as $key => $item) {
            if (Is::notEmpty($item) && in_array($item, $items, true) === false) {
                $data[$key] = $item;
            }
        }
        return $data;
    }
}

