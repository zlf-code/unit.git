<?php

declare(strict_types=1);

namespace Zlf\Unit;

/**
 * 目录助手
 */
class Directory
{

    /**
     * 创建目录
     * @param string $dir 目录路径
     * @param int $permissions 目录权限
     * @return bool
     */
    public static function created(string $dir, int $permissions = 0755): bool
    {
        return is_dir($dir) or self::created(dirname($dir)) and mkdir($dir, $permissions);
    }


    /**
     * 遍历目录下的文件
     * @author 竹林风@875384189 2022/1/27 14:30
     */
    public static function filesInDirectory(string $dir): array
    {
        $files = [];
        $file = scandir($dir);
        foreach ($file as $item) {
            if ($item === '.' || $item === '..') continue;
            if (is_file($dir . '/' . $item)) {
                $files[] = $dir . '/' . $item;
            } elseif (is_dir($dir . '/' . $item)) {
                $files = array_merge($files, self::filesInDirectory($dir . '/' . $item));
            }
        }
        return array_unique($files);
    }


    /**
     * 删除文件夹
     * @param string $dir 要删除的文件夹路径
     * @return bool
     */
    public static function remove(string $dir): bool
    {
        $dh = opendir($dir);
        while ($file = readdir($dh)) {
            if ($file != "." && $file != "..") {
                $path = $dir . "/" . $file;
                if (!is_dir($path)) {
                    unlink($path);
                } else {
                    self::remove($path);
                }
            }
        }
        closedir($dh);
        return rmdir($dir);
    }
}