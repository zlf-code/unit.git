<?php
declare(strict_types=1);

namespace Zlf\Unit;


use Exception;

/**
 * JSON助手
 */
class Json
{

    /**
     * 数组序列化为JSON
     * @param array|object|null $value
     * @param int $flags
     * @param int $depth
     * @return string
     */
    public static function encode($value, int $flags = 0, int $depth = 512): string
    {
        if (is_null($value)) {
            return '[]';
        }
        if (extension_loaded('json')) {
            return json_encode($value, $flags, $depth);
        }
        return '[]';
    }


    /**
     * json解码
     * @param $jsonString
     * @return array
     */
    public static function decode($jsonString): array
    {
        try {
            if (extension_loaded('json')) {
                $data = json_decode($jsonString, true);
                if (is_array($data)) {
                    return $data;
                }
            }
            return [];
        } catch (Exception $exception) {
            return [];
        }
    }
}