<?php

declare(strict_types=1);

namespace Zlf\Unit;

/**
 * 运算计算
 */
class Compute
{
    /**
     * 计算两个数的公倍数
     * @param array $list
     * @return int
     */
    public static function publicTimes(array $list): int
    {
        $decimalLength = 0;//公共倍数
        foreach ($list as $item) {
            $nums = explode(',', (string)$item);
            if (count($nums) === 2 && strlen($nums[1]) > 0) {
                $decimalLength = max(strlen($nums[1]), $decimalLength);
            }
        }
        $times = 1;
        for ($i = 0; $i < $decimalLength; $i++) {
            $times = $times * 10;
        }
        return $times;
    }


    /**
     * 集中运算
     * @param $num1
     * @param $num2
     * @param string $type
     * @return float|int|void
     */
    private static function Compute($num1, $num2, string $type)
    {
        $times = self::publicTimes([$num1, $num2]);
        switch ($type) {
            case '+':
                return ((floatval($num1) * $times) + (floatval($num2) * $times)) / $times;
            case '-':
                return ((floatval($num1) * $times) - (floatval($num2) * $times)) / $times;
            case '*':
                return ((floatval($num1) * $times) * (floatval($num2) * $times)) / ($times * $times);
            case '/':
                return ((floatval($num1) * $times) / (floatval($num2) * $times)) / $times;
        }
    }


    /**
     * 加法
     * @param $num1
     * @param $num2
     * @return float|int
     */
    public static function Addition($num1, $num2)
    {
        return self::Compute($num1, $num2, '+');
    }


    /**
     * 减法
     * @param $num1
     * @param $num2
     * @return float|int
     */
    public static function Subtract($num1, $num2)
    {
        return self::Compute($num1, $num2, '-');
    }


    /**
     * 乘法
     * @param $num1
     * @param $num2
     * @return float|int
     */
    public static function Multiply($num1, $num2)
    {
        return self::Compute($num1, $num2, '*');
    }


    /**
     * 除法
     * @param $num1
     * @param $num2
     * @return float|int
     */
    public static function Divide($num1, $num2)
    {
        return self::Compute($num1, $num2, '/');
    }
}