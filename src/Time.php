<?php
declare(strict_types=1);

namespace Zlf\Unit;

class Time
{

    /**
     * 获取时间戳
     * @param int $deviation
     * @return int
     */
    public static function timestamp(int $deviation = 0): int
    {
        return time() + $deviation;
    }


    /**
     * 获取日期时间值
     * @param string|int $time
     * @return int
     */
    public static function value($time): int
    {
        $time = strval($time);
        if (preg_match('/^(\d{10})$/', $time)) {
            return intval($time);
        } elseif (preg_match('/^(\d{13})$/', $time)) {
            return intval(intval($time) / 1000);
        }
        return strtotime($time);
    }


    /**
     * 获取毫秒时间戳
     * @return int
     */
    public static function microtime(): int
    {
        $time = microtime(true);
        return (int)round($time * 1000);
    }


    /**
     * 获取回调内的运行时间
     * @param callable $callback
     * @return int
     */
    public static function getRunTime(callable $callback): int
    {
        $start = self::microtime();
        $callback();
        return self::microtime() - $start;
    }
}