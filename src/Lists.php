<?php
declare(strict_types=1);

namespace Zlf\Unit;

/**
 * 数组工具类
 */
class Lists
{
    /**
     * 将列表转为字符列表
     * @param array $list
     * @return array
     */
    public static function toStringList(array $list): array
    {
        foreach ($list as &$item) {
            $item = (string)$item;
        }
        return $list;
    }


    /**
     * 将列表转为整型列表
     * @param array $list
     * @return array
     */
    public static function toIntList(array $list): array
    {
        foreach ($list as &$item) {
            $item = (int)$item;
        }
        return $list;
    }


    /**
     * 删除列表中的指定值
     * @param array $list
     * @param mixed $value
     * @return array
     */
    public static function deleteValue($list, $value): array
    {
        $data = [];
        foreach ($list as $item) {
            if ($item !== $value) {
                $data[] = $item;
            }
        }
        return $data;
    }


    /**
     * 切割列表
     * @param array $array
     * @param int $len 每多少个数组切割为一个新的数组
     * @return array
     */
    public static function cutting(array $array, int $len): array
    {
        $newsData = [];
        $tmp = [];
        foreach ($array as $sub) {
            $tmp[] = $sub;
            if (count($tmp) === $len) {
                $newsData[] = $tmp;
                $tmp = [];
            }
        }
        if (count($tmp) > 0) {
            $newsData[] = $tmp;
        }
        return $newsData;
    }


    /**
     * 是否存在重复值
     * @param array $attrList
     * @return bool
     */
    public static function existsRepeat(array $attrList): bool
    {
        return count($attrList) !== count(array_unique($attrList));
    }


    /**
     * 检测字符串列表中是否含有空字符串
     * @param array $list
     * @return bool
     */
    public static function existsEmptyString(array $list): bool
    {
        foreach ($list as $item) {
            if (empty($item) && $item !== '0') {
                return true;
            }
        }
        return false;
    }


    /**
     * 数字集合转字符集合
     * @param array $list
     * @return array
     */
    public static function numToStr(array $list): array
    {
        $news = [];
        foreach ($list as $item) {
            $news[] = (string)$item;
        }
        return $news;
    }


    /**
     * 获取列表最后一个元素
     * @param array $array
     * @return mixed
     */
    public static function last(array $array)
    {
        return count($array) === 0 ? null : $array[count($array) - 1];
    }


    /**
     * 获取列表属性
     * @param array $array
     * @return array
     */
    public static function getColumnsKey(array $array): array
    {
        $data = [];
        if (count($array) > 0) {
            $data = array_keys($array[0]);
        }
        return $data;
    }


    /**
     * 列表插队
     * @param array $data
     * @param array $new_data
     * @param string $name
     * @return array
     */
    public static function CutInLine(array $data, array $new_data, int $sort): array
    {
        $news = [];
        foreach ($data as $index => $value) {
            $news[] = $value;
            if ($index === $sort) {
                foreach ($new_data as $nval) {
                    $news[] = $nval;
                }
            }
        }
        return $news;
    }


    /**
     * 删除空元素
     * @param array $list
     * @param array $items 指定需要排除的元素
     * @return array
     */
    public static function removeEmptyValue(array $list, array $items = []): array
    {
        $data = [];
        foreach ($list as $item) {
            if (Is::notEmpty($item) && in_array($item, $items, true) === false) {
                $data[] = $item;
            }
        }
        return $data;
    }


    /**
     * 判断两个列表元素是否一致，不考虑顺序
     * @param array $list1
     * @param array $list2
     * @return bool
     */
    public static function ComparingElements(array $list1, array $list2): bool
    {
        if (count($list1) !== count($list2)) {
            return false;
        }
        foreach ($list1 as $item) {
            if (in_array($item, $list2, true) === false) {
                return false;
            }
        }
        return true;
    }


    /**
     * 列表元素映射值列表
     * @param array $list 列表值，元素组成部分需要是string或number [1,2,3]
     * @param array $map 字典值，是一个map元素  ['1'=>'一'，'2'=>'二','3'=>'三']
     * @param bool $unique 元素去重
     * @return array  ['一'，'二','三']
     */
    public static function elementMappingLabelsList(array $list, array $map, bool $unique = true): array
    {
        $newsData = [];
        foreach ($list as $el) {
            if (isset($map[$el])) {
                $newsData[] = $map[$el];
            }
        }
        return $unique ? Arr::unique($newsData) : $newsData;
    }


    /**
     * 从列表随机取出一个元素
     * @param array $list
     * @return mixed
     */
    public static function rand(array $list)
    {
        return $list[rand(0, count($list) - 1)];
    }
}

