<?php
declare(strict_types=1);

namespace Zlf\Unit;

class Map
{
    /**
     * 向数组插队
     * @param array $data
     * @param array $new_data
     * @param string $name
     * @return array
     */
    public static function CutInLine(array $data, array $new_data, string $name): array
    {
        $news = [];
        foreach ($data as $key => $value) {
            $news[$key] = $value;
            if ($key === $name) {
                foreach ($new_data as $nkey => $nval) {
                    $news[$nkey] = $nval;
                }
            }
        }
        return $news;
    }


    /**
     * 有效值合并,优先取用数组1的值
     * @param array $array1
     * @param array $array2
     * @return array
     */
    public static function EffectiveMerger(array $array1, array $array2): array
    {
        $keys = Arr::unique(array_merge(array_keys($array1), array_keys($array2)));
        $data = [];
        foreach ($keys as $key) {
            if (isset($array1[$key]) && Is::notEmpty($array1[$key])) {
                $data[$key] = $array1[$key];
            } elseif (isset($array2[$key]) && Is::notEmpty($array2[$key])) {
                $data[$key] = $array2[$key];
            }
        }
        return $data;
    }
}