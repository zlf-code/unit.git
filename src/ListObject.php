<?php
declare(strict_types=1);

namespace Zlf\Unit;

/**
 * @describe 列表数组处理
 * @user zlf <zms-pro@qq.com> 2025/1/16
 */
class ListObject
{
    /**
     * @describe 写入属性
     * @user zlf <zms-pro@qq.com> 2025/1/16
     * @param array $ListObject
     * @param string $key
     * @param $value
     * @return array
     */
    public static function WriteProperties(array $ListObject, string $key, $value): array
    {
        foreach ($ListObject as &$item) {
            $item[$key] = $value;
        }
        return $ListObject;
    }
}