<?php
declare(strict_types=1);

namespace Zlf\Unit;

class Number
{

    /**
     * 字符串转换数字
     * @param string|float|int $number
     * @return float|int|null
     */
    public static function format($number)
    {
        if (is_numeric($number)) {
            if (is_string($number)) {
                return preg_match('/\./', $number) ? (floatval($number)) : (intval($number));
            }
            return $number;
        } else {
            return null;
        }
    }


    /**
     * 获取数字精度
     * @param int|float|string $number
     * @return int
     */
    public static function precision($number): int
    {
        $data = self::format($number);
        if (is_int($data)) {
            return 0;
        }
        $info = Str::explode('.', strval($data));
        return isset($info[1]) ? strlen($info[1]) : 0;
    }


    /**
     * 保留精度
     * @param string|int|float $number 数字
     * @param int $precision 精度
     * @param string $abnormalDefaultValue 数字
     * @return string
     */
    public static function RetainPrecision($number, $precision = 2, $abnormalDefaultValue = '0'): string
    {
        if (is_numeric($number)) {
            $info = Str::explode('.', strval($number));
            //截取字符串前两位
            $info[1] = substr("{$info[1]}000000000000000000", 0, $precision);
            return implode('.', $info);
        }
        return $abnormalDefaultValue;
    }


    /**
     * 保留数字精度转浮点数
     * @param string|int|float $number
     * @param int $precision
     * @param string $abnormalDefaultValue
     * @return float
     */
    public static function RetainFloatPrecision($number, $precision = 2, $abnormalDefaultValue = '0'): float
    {
        $num = self::RetainPrecision($number, $precision, $abnormalDefaultValue);
        return floatval($num);
    }
}