<?php

declare(strict_types=1);

namespace Zlf\Unit;

class ArrFormat
{
    /**
     * 把字典组转为表单格式
     * 格式为 ['value'=>'','label'=>'']
     * @param array $array 字典数据
     * @param bool $valueToString 是否把 $value转为string
     * @return array
     */
    public static function mapToFormdata(array $array, bool $valueToString = true): array
    {
        $data = [];
        foreach ($array as $value => $label) {
            $data[] = ['value' => $valueToString ? ((string)$value) : $value, 'label' => $label];
        }
        return $data;
    }


    /**
     * 把二维数组转为表单格式
     * @param array $array 二维数组
     * @param string $valueKey value对应的键名
     * @param string $labelKey label 对应的键名称
     * @param bool $valueToString value是否转string
     * @param false $KeepOthers 是否保留其他字段
     * @return array
     */
    public static function arrToFormdata(array $array, string $valueKey = 'id', string $labelKey = 'name', bool $valueToString = true, bool $KeepOthers = false): array
    {
        $data = [];
        foreach ($array as $row) {
            $row['value'] = $valueToString ? ((string)$row[$valueKey]) : $$row[$valueKey];
            $row['label'] = $row[$labelKey];
            unset($row[$valueKey]);
            unset($row[$labelKey]);
            $data[] = $KeepOthers ? $row : ['value' => $row['value'], 'label' => $row['label']];
        }
        return $data;
    }
}
