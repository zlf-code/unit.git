<?php
declare(strict_types=1);

namespace Zlf\Unit;

use Exception;

/**
 * 数据加密解密
 */
class Encryption
{
    /**
     * 数据加密
     */
    public static function encrypt(mixed $data, ?string $key = null): string
    {
        $data = openssl_encrypt(json_encode(['data' => $data]), 'AES-128-ECB', $key, OPENSSL_RAW_DATA);
        return base64_encode($data);
    }


    /**
     * 数据解密
     */
    public static function decrypt(string $str, ?string $key = null)
    {
        try {
            $data = openssl_decrypt(base64_decode($str), 'AES-128-ECB', $key, OPENSSL_RAW_DATA);
            if (!$data) return null;
            $arr = Json::decode($data);
            return $arr['data'] ?? null;
        } catch (Exception $exception) {
            return null;
        }
    }
}