<?php
declare(strict_types=1);

namespace Zlf\Unit;

use Exception;

/**
 * 文件工具
 * Class Arr
 * @package Core\Method
 */
class File
{
    /**
     * 持续写入文件
     * @param string $file
     * @param string $content
     * @return string|null
     */
    public static function appendWrite(string $file, string $content): ?string
    {
        try {
            if (!file_exists($file)) {
                $fileDir = dirname($file);
                if (!is_dir($fileDir)) {
                    if (!Directory::created($fileDir)) {
                        return null;
                    }
                }
            }
            $fileRes = fopen($file, "a+");
            if ($fileRes) {
                fwrite($fileRes, $content);
                fclose($fileRes);
                return $file;
            }
            return null;
        } catch (Exception $exception) {
            return null;
        }
    }


    /**
     * 更改文件名称
     * @param string $fileName 文件路径
     * @param string|null $newsName 新的文件名称
     * @return string
     */
    public static function changeFileName(string $fileName, ?string $newsName = null): string
    {
        $extend = self::extendName($fileName);
        return dirname($fileName) . '/' . ($newsName ?: Str::md5_16([time(), uniqid()])) . '.' . $extend;
    }


    /**
     * 获取文件扩展名
     * @param string $fileName 文件名称
     * @param bool $lower 是否转小写
     * @return string
     */
    public static function extendName(string $fileName, bool $lower = true): string
    {
        $data = Str::explode('.', $fileName);
        return $lower ? strtolower(Arr::finalValue($data)) : Arr::finalValue($data);
    }


    /**
     * 获取目录大小
     * @param string $dir
     * @return int
     */
    public static function getDirectorySize(string $dir): int
    {
        $sizeResult = 0;
        $handle = opendir($dir);
        while (false !== ($FolderOrFile = readdir($handle))) {
            if ($FolderOrFile != "." && $FolderOrFile != "..") {
                if (is_dir("$dir/$FolderOrFile")) {
                    $sizeResult += self::getDirectorySize("$dir/$FolderOrFile");
                } else {
                    $sizeResult += filesize("$dir/$FolderOrFile");
                }
            }
        }
        closedir($handle);
        return $sizeResult;
    }
}