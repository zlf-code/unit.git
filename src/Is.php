<?php

declare(strict_types=1);

namespace Zlf\Unit;

use Exception;

class Is
{

    /**
     * 判断字符串是否JSON
     * @param $data
     * @return bool
     */
    public static function json($data): bool
    {
        if (gettype($data) !== 'string') {
            return false;
        }
        try {
            if (extension_loaded('json')) {
                $value = json_decode($data);
                if (is_null($value) || json_last_error()) {
                    return false;
                }
                return true;
            }
            return false;
        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * 判断数组是否是列表
     * @param mixed $list 数据
     * @param ?string $type 列表类型
     * @return bool
     */
    public static function list($list, $type = null): bool
    {
        if (!is_array($list)) {
            return false;
        }
        $sort = 0;
        foreach ($list as $index => $item) {
            if ($sort != $index) {
                return false;
            }
            if ($type && gettype($item) !== $type) {
                return false;
            }
            $sort++;
        }
        return true;
    }

    /**
     * 判断值是否非空值,只判断了字符串,数组,数字
     * @param mixed $value
     * @return bool
     */
    public static function notEmpty($value): bool
    {
        if (is_array($value)) {
            return count($value) > 0;
        } elseif (is_string($value)) {//非字符和数组类型视空
            return strlen($value) > 0 && $value !== ' ';
        } elseif (is_numeric($value)) {//非字符和数组类型视空
            return true;
        }
        return false;
    }


    /**
     * 判断内容是否为空
     * @param $value
     * @return bool
     */
    public static function empty($value): bool
    {
        if (empty($value)) {
            if ($value === 0 || $value === '0') {
                return false;
            }
            return true;
        }
        return false;
    }
}