<?php
declare(strict_types=1);

namespace Zlf\Unit;

/**
 * 对象工具类
 */
class Obj
{
    /**
     * 对象转数组
     * @param object|array $obj
     * @return array
     */
    public static function toArray($obj): array
    {
        if (gettype($obj) === 'array') {
            return $obj;
        }
        return Json::decode(Json::encode($obj));
    }


    /**
     * 重新映射一维键值对数组的键名
     * @param array $data 要处理的列表
     * @param array $mapping 要重新映射的值
     * @param bool $only_reserved_mapping 是否保留未指定的键值
     * @return array
     */
    public static function mapping(array $data, array $mapping, bool $only_reserved_mapping = true): array
    {
        $newsData = [];
        foreach ($data as $key => $value) {
            if (isset($mapping[$key])) {
                $newsData[$mapping[$key]] = $value;
            } elseif ($only_reserved_mapping) {
                $newsData[$key] = $value;
            }
        }
        return $newsData;
    }
}

