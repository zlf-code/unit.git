<?php
declare(strict_types=1);

namespace Zlf\Unit;

class Column
{
    /**
     * 数组对象转换
     * @param array $list 数据
     * @param string $key 释义的字段
     * @param callable $callable 获取释义内容
     * @param string|null $new_find 新的key
     * @param string $default 默认中
     * @param bool $remove_old 删除旧的字段
     * @return array
     */
    public static function explain(array $list, callable $callable, string $key = 'id', ?string $new_find = null, string $default = '', bool $remove_old = false): array
    {
        if (count($list) === 0) {
            return $list;
        }
        $new_find = $new_find ?: "{$key}_name";
        $keys = array_column($list, $key);
        $result = $callable($keys);
        foreach ($list as $index => $item) {
            $list[$index][$new_find] = $result[$item[$key]] ?? $default;
            if ($remove_old) {
                unset($list[$index][$key]);
            }
        }
        return $list;
    }
}