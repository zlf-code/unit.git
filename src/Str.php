<?php
declare(strict_types=1);

namespace Zlf\Unit;

class Str
{

    /**
     * 字符串分割,过滤空数据
     * @param string $separator
     * @param string $string
     * @param bool $is_empty
     * @return array
     */
    public static function explode(string $separator, string $string, bool $is_empty = false): array
    {
        $data = explode($separator, $string);
        if ($is_empty) return $data;
        $newsData = [];
        foreach ($data as $item) {
            if (strlen($item) > 0) {
                $newsData[] = $item;
            }
        }
        return $newsData;
    }


    /**
     * 下划线转驼峰
     * @param string $string
     * @param string $symbol
     * @return string
     * @author 竹林风@875384189 2021/9/6 11:55
     */
    public static function doubleToHump(string $string, string $symbol = '_'): string
    {
        $data = self::explode($symbol, $string);
        $newName = '';
        foreach ($data as $index => $name) {
            if ($index) {
                $newName .= ucfirst($name);
            } else {
                $newName .= $name;
            }
        }
        return $newName;
    }


    /**
     * 驼峰转下划线
     * @param string $string
     * @param string $symbol
     * @return string
     */
    public static function humpToDouble(string $string, string $symbol = '_'): string
    {
        //字符串首字母转小写
        $string = lcfirst($string);
        $newString = '';
        //循环字符串
        for ($i = 0; $i < strlen($string); $i++) {
            //判断是否为大写字母
            if (ord($string[$i]) >= 65 && ord($string[$i]) <= 90) {
                $newString .= $symbol . strtolower($string[$i]);
                //大写转小写
            } else {
                $newString .= $string[$i];
            }
        }
        return $newString;
    }


    /**
     * 不定参数输入生存MD5
     * @param ...$param
     */
    public static function md5(...$param): string
    {
        return md5(http_build_query(['param' => $param]));
    }

    /**
     * 隐藏手机号中间四位
     * @param string $phone
     * @param string $default
     * @return string
     */
    public static function hideMobile(string $phone, string $default = ''): string
    {
        if ($phone) {
            $hiddenPhone = preg_replace('/(\d{3})\d{4}(\d{4})/', '$1****$2', $phone); // 使用正则表达式替换中间四位数字
            return substr_replace($hiddenPhone, '****', 3, 4); // 使用 substr_replace 函数插入隐藏的数字
        }
        return $default;
    }


    /**
     * 生成16位MD5
     */
    public static function md5_16(...$param): string
    {
        $md5 = self::md5(...$param);
        return substr($md5, 8, 16);
    }


    /**
     * 获取class的名称,不包含命名空间
     * @param $className
     * @return string|null
     */
    public static function getClassName($className): ?string
    {
        $data = self::explode("\\", $className);
        if (count($data) > 0) {
            return Arr::finalValue($data);
        }
        return null;
    }


    /**
     * 秒数转时分格式
     * @param  $time int
     * @return string
     */
    public static function Sec2Time(int $time): string
    {
        $value = ['years' => 0, 'days' => 0, 'hours' => 0, 'minutes' => 0];
        if ($time >= 31556926) {
            $value['years'] = floor($time / 31556926);
            $time = ($time % 31556926);
        }
        if ($time >= 86400) {
            $value['days'] = floor($time / 86400);
            $time = ($time % 86400);
        }
        if ($time >= 3600) {
            $value['hours'] = floor($time / 3600);
            $time = ($time % 3600);
        }
        if ($time >= 60) {
            $value['minutes'] = floor($time / 60);
            $time = ($time % 60);
        }
        $value['seconds'] = floor($time);
        return $value['years'] . '年' . $value['days'] . '天 ' . $value["hours"] . '小时' . $value['minutes'] . '分' . $value['seconds'] . '秒';
    }


    /**
     * 限制字符串长度，多余的去掉
     * @param string $string
     * @param int $len
     * @param string $ellipsis
     * @return string
     */
    public static function ellipsis(string $string, int $len, string $ellipsis = '...'): string
    {
        if (mb_strlen($string) > $len) {
            return mb_substr($string, 0, $len) . $ellipsis;
        }
        return $string;
    }


    /**
     * 渲染字符串和参数
     * @param string $string
     * @param array $param
     * @return array|string|string[]
     */
    public static function render(string $string, array $param = [])
    {
        foreach ($param as $key => $value) {
            $string = str_replace("{{$key}}", strval($value), $string);
        }
        return $string;
    }


    /**
     * 字符串相乘
     * @param string $string
     * @param int $num
     * @return string
     */
    public static function multiplication(string $string, int $num): string
    {
        $str = '';
        for ($i = 0; $i < $num; $i++) {
            $str .= $string;
        }
        return $str;
    }
}